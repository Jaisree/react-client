import ApolloBoost, { gql } from 'apollo-boost';
const client = new ApolloBoost({
    uri: 'https://graphql-server.jerilcj1.now.sh',
});
const users = gql`
   query {
     getUsers {
      name{
         title
      }
      location {
         city
         state
      }
      gender
      email
      phone    
    }
  } 
`
client.query({
    query: users
}).then((response) => {
    let html = ''
    response.data.getUsers.forEach((user) => {
        html += `
           <div>
              <h3>Title: ${user.name.title} </h3>
              <h3>City: ${user.location.city} </h3>              
              <h3>State: ${user.location.state} </h3>
              <h3>Gender: ${user.gender} </h3>
              <h3>Email: ${user.email} </h3>
              <h3>Phone: ${user.phone} </h3>
              <hr>
           </div>
        `
    })
    document.getElementById('users').innerHTML = html
})