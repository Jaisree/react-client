# Introduction

create-react-app uses nginx to server the static files. When you run `npm run start` a build folder called dist is created which is copied to the nginx directory www. Then nginx serves those files.

Note: The reason why nodejs app or python does not require nginx is because it has inbuilt webserver. nodejs has express and python uses cherrypy.

# Running the application for development purposes or locally

1. npm install
2. npm run start

# Running the application as a Docker (For production environment only or to host it in other platforms like Digital Ocean or Google Cloud)

1. docker build -t nginx-react-heroku:latest .
2. docker run -d -p 8080:8080 nginx-react-heroku

Note: Before running as a Docker, make the below changes 

In nginx.conf 

Remove the below line

```
server {
        # listen on port 80
        listen $PORT;
```

with 

```
server {
        # listen on port 8080
        listen 8080;
```

In Dockerfile

Remove the below line

```
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/nginx.conf && nginx -g 'daemon off;'
```

and replace it with

```
EXPOSE 8080

ENTRYPOINT ["nginx","-g","daemon off;"]

```

# Push Docker image to Heroku

1. heroku container:login
2. heroku create <PROVIDE APP NAME HERE>
3. heroku container:push web --app <PROVIDE APP NAME HERE>
4. heroku container:release web --app <PROVIDE APP NAME HERE>
5. Access the application on https://<APP NAME HERE>.herokuapp.com